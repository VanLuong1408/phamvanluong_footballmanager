package com.gem.footballmanager.dao;

import com.gem.footballmanager.model.Fixture;
import com.gem.footballmanager.model.Team;

import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
public interface FixtureDAO {

    List<Fixture> getAllResultFixture();

    List<Fixture> getAllFutureFixture();

    void deleteFixture(int id);

    void updateFixture(Fixture fixture);
}
