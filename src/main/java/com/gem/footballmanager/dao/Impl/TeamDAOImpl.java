package com.gem.footballmanager.dao.Impl;

import com.gem.footballmanager.HbUtil;
import com.gem.footballmanager.dao.TeamDAO;
import com.gem.footballmanager.model.BaseModel;
import com.gem.footballmanager.model.Players;
import com.gem.footballmanager.model.Team;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
@Repository("teamDAO")
public class TeamDAOImpl implements TeamDAO {

    @Override
    @Transactional
    public List<Team> getAllTeam() {
        Session session= HbUtil.getSessionFactory().openSession();
        session.beginTransaction();
        String sql="Select t from Team t";


        List<Team> teamList=  session.createQuery(sql).list();

        session.close();

        return teamList;
    }

    @Override
    public void deleteTeam(int id) {
        Session session = HbUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        BaseModel baseModel= (BaseModel) session.get(Team.class, id);
        session.delete(baseModel);

        session.createQuery("update Players SET team=null where team.id= :id").setParameter("id", id).executeUpdate();
        session.createQuery("update Fixture SET team1=null where team1.id= :id").setParameter("id", id).executeUpdate();
        session.createQuery("update Fixture SET team2=null where team2.id= :id").setParameter("id", id).executeUpdate();

        transaction.commit();
        session.close();
    }

    @Override
    public void updateTeam(Team team) {
        Session session = HbUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(team);

        transaction.commit();
        session.close();
    }
}
