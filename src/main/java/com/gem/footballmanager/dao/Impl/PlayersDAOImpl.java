package com.gem.footballmanager.dao.Impl;

import com.gem.footballmanager.HbUtil;
import com.gem.footballmanager.dao.PlayersDAO;
import com.gem.footballmanager.model.BaseModel;
import com.gem.footballmanager.model.Players;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
@Repository("playersDAO")
public class PlayersDAOImpl implements PlayersDAO {


    @Override
    @Transactional
    public List<Players> getAllPlayers() {

        Session session= HbUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Players> playersList=  session.createQuery("from Players").list();

        session.close();

        return playersList;
    }

    @Override
    @Transactional
    public List<Players> getPlayersTop() {
        Session session= HbUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Players> playersList=  session.createQuery("from Players p order by p.goal DESC ").list();

        session.close();
        return playersList;
    }

    @Override
    public void deletePlayers(int id) {
        Session session = HbUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Players players= (Players) session.get(Players.class, id);

        players.removePlayerFromFixture();

        session.delete(players);

        session.createQuery("update GoalScored SET players=null where players.id= :id").setParameter("id", id).executeUpdate();


        transaction.commit();
        session.close();
    }

    @Override
    public void updatePlayer(Players players) {
        Session session = HbUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(players);

        transaction.commit();
        session.close();

    }


}
