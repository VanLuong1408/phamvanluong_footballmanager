package com.gem.footballmanager.dao.Impl;

import com.gem.footballmanager.HbUtil;
import com.gem.footballmanager.dao.LeageDAO;
import com.gem.footballmanager.model.BaseModel;
import com.gem.footballmanager.model.Leage;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
@Repository("leageDAO")
public class LeageDAOImpl implements LeageDAO{

    @Override
    @Transactional
    public List<Leage> getAllLeage() {
        Session session= HbUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Leage> leageList=session.createQuery("FROM Leage").list();

        session.close();

        return leageList;
    }

    @Override
    public void deleteLeage(int id) {

        Session session = HbUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Leage leage= (Leage) session.get(Leage.class, id);

        session.delete(leage);

        session.createQuery("update Team SET leage=null where leage.id= :id").setParameter("id", id).executeUpdate();

        transaction.commit();
        session.close();
    }

    @Override
    public void updateLeage(Leage leage) {
        Session session = HbUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(leage);

        transaction.commit();
        session.close();

    }
}
