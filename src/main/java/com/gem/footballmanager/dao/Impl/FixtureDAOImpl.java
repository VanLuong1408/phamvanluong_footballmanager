package com.gem.footballmanager.dao.Impl;

import com.gem.footballmanager.HbUtil;
import com.gem.footballmanager.dao.FixtureDAO;
import com.gem.footballmanager.model.BaseModel;
import com.gem.footballmanager.model.Fixture;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
@Repository("fixtureDAO")
public class FixtureDAOImpl implements FixtureDAO {


    @Override
    public List<Fixture> getAllResultFixture() {
        Session session= HbUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Fixture> fixtureList=session.createQuery("FROM Fixture WHERE fixtureDate < now()").list();

        session.close();
        return fixtureList;
    }

    @Override
    public List<Fixture> getAllFutureFixture() {
        Session session= HbUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Fixture> fixtureList=session.createQuery("FROM Fixture WHERE (fixtureDate> Now())").list();

        session.close();
        return fixtureList;
    }

    @Override
    public void deleteFixture(int id) {
        Session session = HbUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Fixture fixture= (Fixture) session.get(Fixture.class, id);
        fixture.removeFixtureFromPlayers();

        session.delete(fixture);

        transaction.commit();
        session.close();
    }

    @Override
    public void updateFixture(Fixture fixture) {
        Session session = HbUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(fixture);

        transaction.commit();
        session.close();

    }
}
