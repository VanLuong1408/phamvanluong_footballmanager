package com.gem.footballmanager.dao;

import com.gem.footballmanager.model.Leage;

import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
public interface LeageDAO {
    List<Leage> getAllLeage();

    void deleteLeage(int id);

    void updateLeage(Leage leage);
}
