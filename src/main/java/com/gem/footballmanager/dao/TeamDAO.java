package com.gem.footballmanager.dao;

import com.gem.footballmanager.model.Team;

import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
public interface TeamDAO {
    List<Team> getAllTeam();

    void deleteTeam(int id);

    void updateTeam(Team team);

}
