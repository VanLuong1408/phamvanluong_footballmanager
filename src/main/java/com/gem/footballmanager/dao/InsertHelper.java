package com.gem.footballmanager.dao;

import com.gem.footballmanager.HbUtil;
import com.gem.footballmanager.model.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by vanluong on 27/05/2017.
 */
public class InsertHelper {

    private static void saveDB(BaseModel baseModel){
        Session session= HbUtil.getSessionFactory().openSession();
        Transaction transaction= session.beginTransaction();

        try {
            session.save(baseModel);
            transaction.commit();

        }catch (HibernateException er){
            if (transaction!=null){
                transaction.rollback();
            }

            er.printStackTrace();
        }

        finally {
            session.close();

        }
    }

    public static void insertFixture(Fixture fixture){
        saveDB(fixture);
    }

    public static void insertLeage(Leage leage){
        saveDB(leage);
    }

    public static void insertPlayer(Players players){
        saveDB(players);
    }

    public static void insertTeam(Team team){
        saveDB(team);
    }

    public static void insertGoalScored(GoalScored goalScored){
        saveDB(goalScored);
    }

}
