package com.gem.footballmanager.dao;

import com.gem.footballmanager.model.Players;

import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
public interface PlayersDAO {

    List<Players> getAllPlayers();

    List<Players> getPlayersTop();

    void deletePlayers(int id);

    void updatePlayer(Players players);


}
