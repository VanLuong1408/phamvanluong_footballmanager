package com.gem.footballmanager.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.footballmanager.model.Leage;
import com.gem.footballmanager.model.Team;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
public class TeamDTO extends BaseDTO {

    private String teamName;

    private int numberOfWin;

    private int numberOfLost;

    private int numberOfDraw;

    private int score;

    private String logo;

    private LeageDTO leage;

    private int numberOfScores;

    public TeamDTO(Team team){
        this.teamName=team.getTeamName();
        this.numberOfWin=team.getNumberOfWin();
        this.numberOfLost=team.getNumberOfLost();
        this.numberOfDraw=team.getNumberOfDraw();
        this.score=team.getScore();
        this.logo=team.getLogo();
        ObjectMapper objectMapper=new ObjectMapper();
        this.leage=objectMapper.convertValue(team.getLeage(), LeageDTO.class);

        this.numberOfScores=this.numberOfWin*3 + this.numberOfDraw*1;
    }

    public int getNumberOfScores() {
        return numberOfScores;
    }

    public void setNumberOfScores(int numberOfScores) {
        this.numberOfScores = numberOfScores;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getNumberOfWin() {
        return numberOfWin;
    }

    public void setNumberOfWin(int numberOfWin) {
        this.numberOfWin = numberOfWin;
    }

    public int getNumberOfLost() {
        return numberOfLost;
    }

    public void setNumberOfLost(int numberOfLost) {
        this.numberOfLost = numberOfLost;
    }

    public int getNumberOfDraw() {
        return numberOfDraw;
    }

    public void setNumberOfDraw(int numberOfDraw) {
        this.numberOfDraw = numberOfDraw;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public LeageDTO getLeage() {
        return leage;
    }

    public void setLeage(LeageDTO leage) {
        this.leage = leage;
    }
}
