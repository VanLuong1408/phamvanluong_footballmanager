package com.gem.footballmanager.dto;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.footballmanager.model.Players;


/**
 * Created by vanluong on 02/06/2017.
 */
public class PlayersDTO extends BaseDTO{

    private String name;
    private String position;
    private int goal;
    private String photo;
    private int age;
    private TeamDTO team;


    public PlayersDTO(Players players) {
        this.name = players.getName();
        this.position = players.getPosition();
        this.goal =players.getGoal();
        this.photo = players.getPhoto();
        this.age = players.getAge();


        //convert obj
        this.team = new TeamDTO(players.getTeam());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public TeamDTO getTeam() {
        return team;
    }

    public void setTeam(TeamDTO team) {
        this.team = team;
    }

}
