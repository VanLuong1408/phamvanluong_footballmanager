package com.gem.footballmanager.dto;

import javax.persistence.Column;

/**
 * Created by vanluong on 02/06/2017.
 */
public class LeageDTO extends BaseDTO{

    private String leageName;
    private String logo;
    private int prize;

    public String getLeageName() {
        return leageName;
    }

    public void setLeageName(String leageName) {
        this.leageName = leageName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

}
