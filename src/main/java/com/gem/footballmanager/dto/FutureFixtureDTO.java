package com.gem.footballmanager.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.footballmanager.model.Fixture;
import com.gem.footballmanager.model.Team;

import java.util.Date;

/**
 * Created by vanluong on 07/06/2017.
 */
public class FutureFixtureDTO extends BaseDTO {

    private Date fixtureDate;

    private TeamDTO team1;

    private TeamDTO team2;

    public FutureFixtureDTO(Fixture fixture){
        this.fixtureDate=fixture.getFixtureDate();
        this.team1=new TeamDTO(fixture.getTeam1());
        this.team2=new TeamDTO(fixture.getTeam2());
    }

    public Date getFixtureDate() {
        return fixtureDate;
    }

    public void setFixtureDate(Date fixtureDate) {
        this.fixtureDate = fixtureDate;
    }

    public TeamDTO getTeam1() {
        return team1;
    }

    public void setTeam1(TeamDTO team1) {
        this.team1 = team1;
    }

    public TeamDTO getTeam2() {
        return team2;
    }

    public void setTeam2(TeamDTO team2) {
        this.team2 = team2;
    }
}
