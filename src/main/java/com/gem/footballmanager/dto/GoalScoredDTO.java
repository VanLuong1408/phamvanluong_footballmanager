package com.gem.footballmanager.dto;

import com.gem.footballmanager.model.Fixture;
import com.gem.footballmanager.model.Players;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by vanluong on 02/06/2017.
 */
public class GoalScoredDTO extends BaseDTO{
    private int goalTime;
    private Players players;
    private Fixture fixture;

    public int getGoalTime() {
        return goalTime;
    }

    public void setGoalTime(int goalTime) {
        this.goalTime = goalTime;
    }

    public Players getPlayers() {
        return players;
    }

    public void setPlayers(Players players) {
        this.players = players;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public void setFixture(Fixture fixture) {
        this.fixture = fixture;
    }
}
