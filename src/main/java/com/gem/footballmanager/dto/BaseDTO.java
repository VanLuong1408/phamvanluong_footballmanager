package com.gem.footballmanager.dto;

/**
 * Created by vanluong on 02/06/2017.
 */
public class BaseDTO {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
