package com.gem.footballmanager.dto;

import com.gem.footballmanager.model.Fixture;
import java.util.Date;

/**
 * Created by vanluong on 07/06/2017.
 */
public class ResultFixtureDTO extends BaseDTO{

    private int team1Score;

    private int team2Score;

    private Date fixtureDate;

    private TeamDTO team1;

    private TeamDTO team2;


    public ResultFixtureDTO(Fixture fixture){
        this.team1Score=fixture.getTeam1Score();
        this.team2Score=fixture.getTeam2Score();
        this.fixtureDate=fixture.getFixtureDate();

        this.team1=new TeamDTO(fixture.getTeam1());
        this.team2=new TeamDTO(fixture.getTeam2());
    }

    public int getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(int team1Score) {
        this.team1Score = team1Score;
    }

    public int getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(int team2Score) {
        this.team2Score = team2Score;
    }

    public Date getFixtureDate() {
        return fixtureDate;
    }

    public void setFixtureDate(Date fixtureDate) {
        this.fixtureDate = fixtureDate;
    }

    public TeamDTO getTeam1() {
        return team1;
    }

    public void setTeam1(TeamDTO team1) {
        this.team1 = team1;
    }

    public TeamDTO getTeam2() {
        return team2;
    }

    public void setTeam2(TeamDTO team2) {
        this.team2 = team2;
    }
}
