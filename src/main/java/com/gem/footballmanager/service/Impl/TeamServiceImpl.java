package com.gem.footballmanager.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.footballmanager.dao.InsertHelper;
import com.gem.footballmanager.dao.TeamDAO;
import com.gem.footballmanager.dto.TeamDTO;
import com.gem.footballmanager.model.Team;
import com.gem.footballmanager.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
@Service("teamService")
@Transactional
public class TeamServiceImpl  implements TeamService{

    @Autowired
    private TeamDAO teamDAO;

    @Override
    public List<TeamDTO> getAllTeam() {

        List<Team> teamList=teamDAO.getAllTeam();

        List<TeamDTO> teamDTOList=new ArrayList<TeamDTO>();

        for (int i=0; i<teamList.size(); i++){

            TeamDTO teamDTO=new TeamDTO(teamList.get(i));

            teamDTOList.add(teamDTO);
        }


        return teamDTOList;
    }

    @Override
    public List<TeamDTO> getTeamTop() {

        List<TeamDTO> teamDTOList=getAllTeam();
        
        Collections.reverse(teamDTOList);


        return teamDTOList;
    }

    @Override
    public void insertTeam(Team team) {

        InsertHelper.insertTeam(team);
    }

    @Override
    public void deleteTeam(int id) {
        teamDAO.deleteTeam(id);
    }

    @Override
    public void updateTeam(Team team) {
        teamDAO.updateTeam(team);
    }
}
