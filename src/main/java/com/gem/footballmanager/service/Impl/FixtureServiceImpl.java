package com.gem.footballmanager.service.Impl;

import com.gem.footballmanager.dao.FixtureDAO;
import com.gem.footballmanager.dao.InsertHelper;
import com.gem.footballmanager.dto.FutureFixtureDTO;
import com.gem.footballmanager.dto.ResultFixtureDTO;
import com.gem.footballmanager.model.Fixture;
import com.gem.footballmanager.service.FixtureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
@Service("fixtureService")
@Transactional
public class FixtureServiceImpl implements FixtureService {

    @Autowired
    @Resource(name="fixtureDAO")
    private FixtureDAO fixtureDAO;


    @Override
    public List<ResultFixtureDTO> getAllResultFixture() {
        List<Fixture> fixtureList=fixtureDAO.getAllResultFixture();
        List<ResultFixtureDTO> resultFixtureDTOList=new ArrayList<ResultFixtureDTO>();

        for (int i=0; i<fixtureList.size(); i++){

            resultFixtureDTOList.add(new ResultFixtureDTO(fixtureList.get(i)));

        }

        return resultFixtureDTOList;
    }

    @Override
    public List<FutureFixtureDTO> getAllFutureFixture() {
        List<Fixture> fixtureList=fixtureDAO.getAllFutureFixture();
        List<FutureFixtureDTO> futureFixtureDTOList=new ArrayList<FutureFixtureDTO>();

        for (int i=0; i<fixtureList.size(); i++){

            futureFixtureDTOList.add(new FutureFixtureDTO(fixtureList.get(i)));

        }
        int a=futureFixtureDTOList.size();
        int d;

        return futureFixtureDTOList;
    }

    @Override
    public void insertFixture(Fixture fixture) {
        InsertHelper.insertFixture(fixture);

    }

    @Override
    public void deleteFixture(int id) {
        fixtureDAO.deleteFixture(id);
    }

    @Override
    public void updateFixture(Fixture fixture) {
        fixtureDAO.updateFixture(fixture);
    }
}
