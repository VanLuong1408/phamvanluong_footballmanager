package com.gem.footballmanager.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.footballmanager.dao.InsertHelper;
import com.gem.footballmanager.dao.LeageDAO;
import com.gem.footballmanager.dto.LeageDTO;
import com.gem.footballmanager.model.Leage;
import com.gem.footballmanager.service.LeageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
@Service("leageService")
@Transactional
public class LeageServiceImpl implements LeageService {

    @Autowired
    private LeageDAO leageDAO;

    @Override
    public List<LeageDTO> getAllLeage() {
        List<Leage> leageList=leageDAO.getAllLeage();

        List<LeageDTO> leageDTOList=new ArrayList<LeageDTO>();
        for (int i=0; i<leageList.size(); i++){
            ObjectMapper objectMapper=new ObjectMapper();

            leageDTOList.add(objectMapper.convertValue(leageList.get(i), LeageDTO.class));
        }
        return leageDTOList;
    }

    @Override
    public void insertLeage(Leage leage) {
        InsertHelper.insertLeage(leage);
    }

    @Override
    public void deleteLeage(int id) {
        leageDAO.deleteLeage(id);
    }

    @Override
    public void updateLeage(Leage leage) {
        leageDAO.updateLeage(leage);
    }
}
