package com.gem.footballmanager.service.Impl;

import com.gem.footballmanager.dao.InsertHelper;
import com.gem.footballmanager.dao.PlayersDAO;
import com.gem.footballmanager.dto.PlayersDTO;
import com.gem.footballmanager.model.Players;
import com.gem.footballmanager.service.PlayersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
@Service("playersService")
@Transactional
public class PlayersServiceImpl implements PlayersService {

    @Autowired
    private PlayersDAO playersDAO;

    @Override
    public List<PlayersDTO> getAllPlayerDTO() {

        List<Players> playersList=playersDAO.getAllPlayers();

        List<PlayersDTO> playersDTOList=new ArrayList<PlayersDTO>();

        for (int i=0; i<playersList.size(); i++){

            PlayersDTO playersDTO=new PlayersDTO(playersList.get(i));


            playersDTOList.add(playersDTO);

        }


        return playersDTOList;
    }

    @Override
    public List<PlayersDTO> getPlayerDTOTop() {
        List<Players> playersList=playersDAO.getPlayersTop();

        List<PlayersDTO> playersDTOList=new ArrayList<PlayersDTO>();

        for (int i=0; i<playersList.size(); i++){

            PlayersDTO playersDTO=new PlayersDTO(playersList.get(i));


            playersDTOList.add(playersDTO);

        }


        return playersDTOList;
    }

    @Override
    public void insertPlayer(Players players) {
        InsertHelper.insertPlayer(players);
    }

    @Override
    public void deletePlayer(int id) {
        playersDAO.deletePlayers(id);
    }

    @Override
    public void updatePlayer(Players players) {
        playersDAO.updatePlayer(players);
    }


}
