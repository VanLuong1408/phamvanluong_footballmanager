package com.gem.footballmanager.service;

import com.gem.footballmanager.dto.TeamDTO;
import com.gem.footballmanager.model.Team;

import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
public interface TeamService {

    List<TeamDTO> getAllTeam();

    List<TeamDTO> getTeamTop();

    void insertTeam(Team team);

    void deleteTeam(int id);

    void updateTeam(Team team);
}
