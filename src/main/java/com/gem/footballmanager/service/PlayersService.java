package com.gem.footballmanager.service;

import com.gem.footballmanager.dto.PlayersDTO;
import com.gem.footballmanager.model.Players;

import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
public interface PlayersService {

    List<PlayersDTO> getAllPlayerDTO();

    List<PlayersDTO> getPlayerDTOTop();

    void insertPlayer(Players players);

    void deletePlayer(int id);

    void updatePlayer(Players players);

}
