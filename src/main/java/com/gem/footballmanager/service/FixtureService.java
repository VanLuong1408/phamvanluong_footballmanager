package com.gem.footballmanager.service;

import com.gem.footballmanager.dto.FutureFixtureDTO;
import com.gem.footballmanager.dto.ResultFixtureDTO;
import com.gem.footballmanager.model.Fixture;

import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
public interface FixtureService {
    List<ResultFixtureDTO> getAllResultFixture();

    List<FutureFixtureDTO> getAllFutureFixture();

    void insertFixture(Fixture fixture);

    void deleteFixture(int id);

    void updateFixture(Fixture fixture);

}
