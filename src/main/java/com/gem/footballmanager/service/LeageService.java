package com.gem.footballmanager.service;

import com.gem.footballmanager.dto.LeageDTO;
import com.gem.footballmanager.model.Leage;

import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
public interface LeageService {

    List<LeageDTO> getAllLeage();

    void insertLeage(Leage leage);

    void deleteLeage(int id);

    void updateLeage(Leage leage);
}
