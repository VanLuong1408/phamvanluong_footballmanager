package com.gem.footballmanager.model;

import javax.persistence.*;

/**
 * Created by vanluong on 02/06/2017.
 */
@MappedSuperclass
public class BaseModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
