package com.gem.footballmanager.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vanluong on 02/06/2017.
 */
@Entity
@Table(name = "players")
public class Players extends BaseModel {

    @Column(name = "name")
    private String name;

    @Column(name = "position")
    private String position;

    @Column(name = "goal")
    private int goal;

    @Column(name = "photo")
    private String photo;

    @Column(name = "age")
    private int age;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;

    @ManyToMany(mappedBy = "playersSet")
    private Set<Fixture> fixtureSet=new HashSet<Fixture>();

    @PreRemove
    public void removePlayerFromFixture() {
        for (Fixture f : fixtureSet) {
            f.getPlayersSet().remove(this);
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Set<Fixture> getFixtureSet() {
        return fixtureSet;
    }

    public void setFixtureSet(Set<Fixture> fixtureSet) {
        this.fixtureSet = fixtureSet;
    }
}
