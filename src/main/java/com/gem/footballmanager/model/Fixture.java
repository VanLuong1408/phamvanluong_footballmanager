package com.gem.footballmanager.model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vanluong on 02/06/2017.
 */
@Entity
@Table(name = "fixture")
public class Fixture extends BaseModel {

    @Column(name="team1_score")
    private int team1Score;

    @Column(name = "team2_score")
    private int team2Score;

    @Column(name = "fixture_date")
    private Date fixtureDate;

    @ManyToOne
    @JoinColumn(name = "team1_id")
    private Team team1;

    @ManyToOne
    @JoinColumn(name = "team2_id")
    private Team team2;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "player_match", joinColumns = {
            @JoinColumn(name = "fixture_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "player_id")}
                )
    private Set<Players> playersSet=new HashSet<Players>();

    @PreRemove
    public void removeFixtureFromPlayers() {
        for (Players p : playersSet) {
            p.getFixtureSet().remove(this);
        }
    }

    public int getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(int team1Score) {
        this.team1Score = team1Score;
    }

    public int getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(int team2Score) {
        this.team2Score = team2Score;
    }

    public Date getFixtureDate() {
        return fixtureDate;
    }

    public void setFixtureDate(Date fixtureDate) {
        this.fixtureDate = fixtureDate;
    }

    public Set<Players> getPlayersSet() {
        return playersSet;
    }

    public void setPlayersSet(Set<Players> playersSet) {
        this.playersSet = playersSet;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }
}
