package com.gem.footballmanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by vanluong on 02/06/2017.
 */
@Entity
@Table(name = "leage")
public class Leage extends BaseModel {

    @Column(name = "leage_name")
    private String leageName;

    @Column(name = "logo")
    private String logo;

    @Column(name = "prize")
    private int prize;

    public String getLeageName() {
        return leageName;
    }

    public void setLeageName(String leageName) {
        this.leageName = leageName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }
}
