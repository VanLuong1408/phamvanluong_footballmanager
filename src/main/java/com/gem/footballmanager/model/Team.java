package com.gem.footballmanager.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */

@Entity
@Table(name = "team")
public class Team extends BaseModel {

    @Column(name = "team_name")
    private String teamName;

    @Column(name = "numberofwin")
    private int numberOfWin;

    @Column(name = "numberoflost")
    private int numberOfLost;

    @Column(name = "numberofdraw")
    private int numberOfDraw;

    @Column(name = "score")
    private int score;

    @Column(name = "logo")
    private String logo;

    @ManyToOne
    @JoinColumn(name = "leage_id")
    private Leage leage;


    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getNumberOfWin() {
        return numberOfWin;
    }

    public void setNumberOfWin(int numberOfWin) {
        this.numberOfWin = numberOfWin;
    }

    public int getNumberOfLost() {
        return numberOfLost;
    }

    public void setNumberOfLost(int numberOfLost) {
        this.numberOfLost = numberOfLost;
    }

    public int getNumberOfDraw() {
        return numberOfDraw;
    }

    public void setNumberOfDraw(int numberOfDraw) {
        this.numberOfDraw = numberOfDraw;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Leage getLeage() {
        return leage;
    }

    public void setLeage(Leage leage) {
        this.leage = leage;
    }


}
