package com.gem.footballmanager.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by vanluong on 05/06/2017.
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.gem.footballmanager")
public class AppConfig {

}
