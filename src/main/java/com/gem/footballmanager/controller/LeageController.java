package com.gem.footballmanager.controller;

import com.gem.footballmanager.dto.LeageDTO;
import com.gem.footballmanager.model.Leage;
import com.gem.footballmanager.service.LeageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
@RestController
@RequestMapping("/leage")
public class LeageController {

    @Autowired
    @Resource(name = "leageService")
    private LeageService leageService;

    @GetMapping("Leages")
    public List<LeageDTO> findAllLeages(){
        return leageService.getAllLeage();
    }

    @PostMapping("Leage")
    public ResponseEntity addLeage(@RequestBody Leage leage){

        leageService.insertLeage(leage);

        return new ResponseEntity(leage,  HttpStatus.OK);
    }

    @DeleteMapping("Leage/{id}")
    public ResponseEntity deleteLeage(@PathVariable int id) {

        leageService.deleteLeage(id);

        return new ResponseEntity(id, HttpStatus.OK);
    }

    @PutMapping("Leage")
    public ResponseEntity updateLeage(@RequestBody Leage leage){
        leageService.updateLeage(leage);

        return new ResponseEntity(leage, HttpStatus.OK);
    }

}
