package com.gem.footballmanager.controller;

import com.gem.footballmanager.dto.PlayersDTO;
import com.gem.footballmanager.model.Leage;
import com.gem.footballmanager.model.Players;
import com.gem.footballmanager.service.PlayersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
@RestController
@RequestMapping("/player")
public class PlayersController {

    @Autowired
    @Resource(name = "playersService")
    private PlayersService playersService;

    @GetMapping("Players")
    public List<PlayersDTO> findAllPlayers(){

        return playersService.getAllPlayerDTO();
    }

    @GetMapping("PlayersTop")
    public List<PlayersDTO> findPlayersTop(){
        return playersService.getPlayerDTOTop();
    }

    @PostMapping("Player")
    public ResponseEntity addPlayer(@RequestBody Players players){

        playersService.insertPlayer(players);

        return new ResponseEntity(players,  HttpStatus.OK);
    }

    @DeleteMapping("Player/{id}")
    public ResponseEntity deletePlayer(@PathVariable  int id){
        playersService.deletePlayer(id);

        return new ResponseEntity(id, HttpStatus.OK);
    }

    @PutMapping("/Player")
    public ResponseEntity updateCustomer(@RequestBody Players players) {

       playersService.updatePlayer(players);

        return new ResponseEntity(players, HttpStatus.OK);
    }


}
