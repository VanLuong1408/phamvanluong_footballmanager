package com.gem.footballmanager.controller;

import com.gem.footballmanager.dto.TeamDTO;
import com.gem.footballmanager.model.Leage;
import com.gem.footballmanager.model.Team;
import com.gem.footballmanager.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by vanluong on 02/06/2017.
 */
@RestController
@RequestMapping("/team")
public class TeamController {

    @Autowired
    @Resource(name = "teamService")
    private TeamService teamService;

    @GetMapping("Teams")
    public List<TeamDTO> findAllTeams(){
        return teamService.getAllTeam();
    }

    @GetMapping("TeamsTop")
    public List<TeamDTO> findAllTeamsTop(){
        return teamService.getTeamTop();
    }

    @DeleteMapping("Team/{id}")
    public ResponseEntity deleteTeam(@PathVariable int id){

        teamService.deleteTeam(id);

        return new ResponseEntity(id, HttpStatus.OK);
    }

    @PostMapping("Team")
    public ResponseEntity addTeam(@RequestBody Team team){

        teamService.insertTeam(team);

        return new ResponseEntity(team,  HttpStatus.OK);
    }

    @PutMapping("Team")
    public ResponseEntity updateCustomer(@RequestBody Team team) {

        teamService.updateTeam(team);

        return new ResponseEntity(team, HttpStatus.OK);
    }
}
