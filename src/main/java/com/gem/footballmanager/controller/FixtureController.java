package com.gem.footballmanager.controller;

import com.gem.footballmanager.dto.FutureFixtureDTO;
import com.gem.footballmanager.dto.ResultFixtureDTO;
import com.gem.footballmanager.model.Fixture;
import com.gem.footballmanager.model.Leage;
import com.gem.footballmanager.service.FixtureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by vanluong on 07/06/2017.
 */
@RestController
@RequestMapping("/fixture")
public class FixtureController  {
    @Autowired
    @Resource(name = "fixtureService")
    private FixtureService fixtureService;


    @GetMapping("ResultFixtures")
    public List<ResultFixtureDTO> findAllResultFixture(){
        return fixtureService.getAllResultFixture();
    }

    @GetMapping("FutureFixtures")
    public List<FutureFixtureDTO> findAllFutureFixture(){
        return fixtureService.getAllFutureFixture();
    }

    @PostMapping("Fixture")
    public ResponseEntity addFixture(@RequestBody Fixture fixture){

        fixtureService.insertFixture(fixture);

        return new ResponseEntity(fixture,  HttpStatus.OK);
    }

    @DeleteMapping("Fixture/{id}")
    public ResponseEntity deleteFixture(@PathVariable int id) {

        fixtureService.deleteFixture(id);

        return new ResponseEntity(id, HttpStatus.OK);
    }

    @PutMapping("Leage")
    public ResponseEntity updateFixture(@RequestBody Fixture fixture){

        fixtureService.updateFixture(fixture);

        return new ResponseEntity(fixture, HttpStatus.OK);
    }
}
