-- MySQL Workbench Forward Engineering

/* SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0; */
/* SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0; */
/* SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'; */

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Table `mydb`.`leage`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS leage (
  id SERIAL NOT NULL  ,
  leage_name VARCHAR(45) NULL,
  logo VARCHAR(45) NULL,
  prize INT NULL,
  PRIMARY KEY (id))
;


-- -----------------------------------------------------
-- Table `mydb`.`team`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS team (
  team_name VARCHAR(45) ,
  numberOfWin INT NULL,
  numberOfLost INT NULL,
  numberOfDraw INT NULL,
  score INT NULL,
  logo VARCHAR(45) NULL,
  id SERIAL NOT NULL ,
  leage_id INT ,
  PRIMARY KEY (id)
 ,
  CONSTRAINT fk_team_leage1
    FOREIGN KEY (leage_id)
    REFERENCES leage (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_team_leage1_idx ON team (leage_id ASC);



-- -----------------------------------------------------
-- Table `mydb`.`players`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS players (
  id SERIAL NOT NULL ,
  name VARCHAR(45) NULL,
  position VARCHAR(45) NULL,
  goal INT NULL,
  photo VARCHAR(45) NULL,
  age INT NULL,
  team_id INT,
  PRIMARY KEY (id)
 ,
  CONSTRAINT fk_players_team
    FOREIGN KEY (team_id)
    REFERENCES team (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_players_team_idx ON players (team_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`fixture`
-- -----------------------------------------------------
CREATE SEQUENCE fixture_seq;

CREATE TABLE IF NOT EXISTS fixture (
  id SERIAL NOT NULL ,
  team1_score INT NULL,
  team2_score INT NULL,
  fixture_date DATE NULL,
  team1_id INT ,
  team2_id INT ,
  PRIMARY KEY (id)
 ,
  CONSTRAINT fk_fixture_team1
    FOREIGN KEY (team1_id)
    REFERENCES team (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_fixture_team2
    FOREIGN KEY (team2_id)
    REFERENCES team (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_fixture_team1_idx ON fixture (team1_id ASC);
CREATE INDEX fk_fixture_team2_idx ON fixture (team2_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`goal_scored`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS goal_scored (
  id SERIAL NOT NULL ,
  goal_time INT NULL,
  player_id INT ,
  fixture_id INT,
  PRIMARY KEY (id)
 ,
  CONSTRAINT fk_goal_scored_players1
    FOREIGN KEY (player_id)
    REFERENCES players (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_goal_scored_fixture1
    FOREIGN KEY (fixture_id)
    REFERENCES fixture (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_goal_scored_players1_idx ON goal_scored (player_id ASC);
CREATE INDEX fk_goal_scored_fixture1_idx ON goal_scored (fixture_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`player_match`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS player_match (
  goal_conceeded INT  NULL ,
  min_played INT NULL,
  yellow_card INT NULL,
  red_card INT NULL,
  fixture_id INT,
  player_id INT
 ,
  CONSTRAINT fk_player_match_fixture1
    FOREIGN KEY (fixture_id)
    REFERENCES fixture (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_player_match_players1
    FOREIGN KEY (player_id)
    REFERENCES players (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_player_match_fixture1_idx ON player_match (fixture_id ASC);
CREATE INDEX fk_player_match_players1_idx ON player_match (player_id ASC);


/* SET SQL_MODE=@OLD_SQL_MODE; */
/* SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS; */
/* SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS; */
